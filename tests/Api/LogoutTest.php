<?php

namespace Tests\Api;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions as Transaction;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use Transaction;

    private static $endpoint;

    public function setUp()
    {
        parent::setUp();

        factory(User::class)->create([
            'email' => 'gaga@example.com',
            'password' => bcrypt('password')
        ]);
    }

    public static function setUpBeforeClass()
    {
        self::$endpoint = '/api/v1/logout';
    }

    public static function tearDownAfterClass()
    {
        self::$endpoint = null;
    }

    /**
     * Tests the POST /logout route
     * Successful logout
     *
     * @return void
     */
    public function test_can_logout_a_user()
    {
        // Given

        // I have an existing user...

        // ... and I log them in
        $response = $this->json(
            'POST',
            '/api/v1/login',
            [
                'email' => 'gaga@example.com',
                'password' => 'password'
            ],
            ['Accept' => 'application/json']
        );
        $access_token = $response->getData()->access_token;

        // When I logout the user with an invalid token
        // Then
        // The response status should be 401
        $this
            ->withHeaders([
                'Authorization' => 'Bearer INVALID_TOKEN'
            ])
            ->json('POST', self::$endpoint)
            ->assertStatus(401);

        // When I logout the user
        // Then
        // The response status should be 204
        $this
            ->withHeaders([
                'Authorization' => "Bearer {$access_token}"
            ])
            ->json('POST', self::$endpoint)
            ->assertStatus(204);
    }

    /**
     * Tests the POST /logout route
     * Failed logout
     *
     * @return void
     */
    public function test_logout_returns_401_if_user_is_not_logged_in()
    {
        // Given

        // I try to access a guarded route with an invalid token...

        // Then
        // The response status should be 401
        $access_token = 'INVALID_TOKEN';
        $this
            ->withHeaders([
                'Authorization' => "Bearer {$access_token}"
            ])
            ->json('GET', '/api/v1/user')
            ->assertStatus(401);
    }
}
