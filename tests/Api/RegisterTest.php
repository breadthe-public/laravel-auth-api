<?php

namespace Tests\Api;

use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions as Transaction;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use Transaction;

    private static $endpoint;
    private static $now;

    public static function setUpBeforeClass()
    {
        self::$endpoint = '/api/v1/register';
        self::$now = Carbon::now()->toDateTimeString();
    }

    public static function tearDownAfterClass()
    {
        self::$endpoint = null;
        self::$now = null;
    }

    /**
     * Tests the POST /register route
     *
     * @return void
     */
    public function test_can_register_users()
    {
        // Given

        // I have 2 new user records
        $userOne = [
            'email' => 'gaga1@example.com',
            'password' => 'password1',
            'password_confirmation' => 'password1',
        ];
        $userTwo = [
            'email' => 'gaga2@example.com',
            'password' => 'password2',
            'password_confirmation' => 'password2',
        ];

        // And I register the users
        $response1 = $this->json('POST', self::$endpoint, $userOne);
        $response2 = $this->json('POST', self::$endpoint, $userTwo);

        // When

        // I fetch the users...
        // ... and their passwords; make the hidden "password" field visible
        $users = User::get(['email', 'created_at', 'updated_at'])->toArray();
        $user_passwords = User::get(['password'])->makeVisible('password')->toArray();

        // Then

        // The response should be 200
        $response1->assertStatus(200);
        $response2->assertStatus(200);

        // There should be 2 users created
        $this->assertCount(2, $users);

        // The response should be in the proper format

        $this->assertEquals([
            [
                'email' => $userOne['email'],
                'created_at' => self::$now,
                'updated_at' => self::$now,
            ],
            [
                'email' => $userTwo['email'],
                'created_at' => self::$now,
                'updated_at' => self::$now,
            ],
        ], $users);

        // The passwords should match
        $this->assertTrue(Hash::check($userOne['password'], $user_passwords[0]['password']));
        $this->assertTrue(Hash::check($userTwo['password'], $user_passwords[1]['password']));
    }

    public function test_registration_fails_if_fields_are_invalid()
    {
        // Given

        // I have an existing user
        factory(User::class)->create([
            'email' => 'gaga@example.com'
        ]);

        // And I'm trying to create a new user with an existing email
        $newUser1 = [
            'email' => 'gaga@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ];

        // And I'm trying to create a new user with an invalid email
        $newUser2 = [
            'email' => '',
            'password' => 'password',
            'password_confirmation' => 'password',
        ];

        // And I'm trying to create a new user with a password that doesn't match the password confirmation
        $newUser3 = [
            'email' => 'gaga2@example.com',
            'password' => 'password',
            'password_confirmation' => 'password0',
        ];

        // And I'm trying to create a new user with a password that is < 6 characters
        $newUser4 = [
            'email' => 'gaga3@example.com',
            'password' => 'pass',
            'password_confirmation' => 'pass',
        ];

        // And I register the new users
        $response1 = $this->json('POST', self::$endpoint, $newUser1);
        $response2 = $this->json('POST', self::$endpoint, $newUser2);
        $response3 = $this->json('POST', self::$endpoint, $newUser3);
        $response4 = $this->json('POST', self::$endpoint, $newUser4);

        // When

        // I fetch the users
        $users = User::get()->toArray();

        // Then

        // The responses should all be 400
        $response1->assertStatus(400);
        $response2->assertStatus(400);
        $response3->assertStatus(400);
        $response4->assertStatus(400);

        // And there should only be 1 user in the DB
        $this->assertCount(1, $users);
    }
}
