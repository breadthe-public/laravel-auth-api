<?php

namespace Tests\Api;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions as Transaction;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ResetPasswordTest extends TestCase
{
    use Transaction;

    private static $endpoint;

    public function setUp()
    {
        parent::setUp();

        factory(User::class)->create([
            'email' => 'gaga@example.com',
            'password' => bcrypt('password')
        ]);
    }

    public static function setUpBeforeClass()
    {
        self::$endpoint = '/api/v1/password/reset';
    }

    public static function tearDownAfterClass()
    {
        self::$endpoint = null;
    }

    /**
     * Tests the POST /password/forgot route
     * Checks if the correct response statuses are returned
     * when requesting a password reset
     *
     * @return void
     */
    public function test_reset_password_response_status()
    {
        // Given

        // I have an existing user who has requested a password reset

        // And I simulate a password reset request
        $email = 'gaga@example.com';
        $token = str_random(32);
        $created_at = Carbon::now();
        DB::table('password_resets')->insert(compact('email', 'token', 'created_at'));

        // Check if the reset token has expired

        // And various invalid scenarios for resetting the password
        $scenarios = [
            // Missing email
            [
                'email' => '',
                'password' => 'password',
                'password_confirmation' => 'password',
                'token' => $token,
                'expected' => [
                    'status' => 400
                ]
            ],
            // Missing password
            [
                'email' => 'gaga@example.com',
                'password' => '',
                'password_confirmation' => 'password',
                'token' => $token,
                'expected' => [
                    'status' => 400
                ]
            ],
            // Passwords don't match
            [
                'email' => 'gaga@example.com',
                'password' => 'password',
                'password_confirmation' => 'password0',
                'token' => $token,
                'expected' => [
                    'status' => 400
                ]
            ],
            // Inexistent user
            [
                'email' => 'gagagaga@example.com',
                'password' => 'password',
                'password_confirmation' => 'password',
                'token' => $token,
                'expected' => [
                    'status' => 401
                ]
            ],
            // Invalid token
            [
                'email' => 'gaga@example.com',
                'password' => 'password',
                'password_confirmation' => 'password',
                'token' => 'INVALID_TOKEN',
                'expected' => [
                    'status' => 401
                ]
            ]
        ];

        // The message should be correct
        array_walk($scenarios, function ($body) {
            $this->json('POST', self::$endpoint, $body, ['Accept' => 'application/json'])
                ->assertStatus($body['expected']['status']);
        });

        // And when the user submits their correct: email, password, password confirmation, token
        // The response status should be 200
        // The message should be correct
        $this
            ->json(
                'POST',
                self::$endpoint,
                [
                    'email' => 'gaga@example.com',
                    'password' => 'password',
                    'password_confirmation' => 'password',
                    'token' => $token
                ],
                ['Accept' => 'application/json']
            )
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Password was successfully reset.'
            ]);
    }

    public function test_reset_password_response_status_for_expired_token()
    {
        // Given

        // I have an existing user who has requested a password reset

        // And I simulate a password reset request with an expired token (25 hours old)
        $email = 'gaga@example.com';
        $token = str_random(32);
        $created_at = Carbon::now()->subHours(25);
        DB::table('password_resets')->insert(compact('email', 'token', 'created_at'));

        // Then

        // When the user submits their correct: email, password, password confirmation, token
        // The response status should be 401
        // The message should be correct
        $this
            ->json(
                'POST',
                self::$endpoint,
                [
                    'email' => 'gaga@example.com',
                    'password' => 'password',
                    'password_confirmation' => 'password',
                    'token' => $token
                ],
                ['Accept' => 'application/json']
            )
            ->assertStatus(401)
            ->assertJson([
                'message' => 'The password reset token has expired.'
            ]);
    }
}
