<?php

namespace Tests\Api;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions as Transaction;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    use Transaction;

    private static $endpoint;

    public function setUp()
    {
        parent::setUp();

        factory(User::class)->create([
            'email' => 'gaga@example.com',
            'password' => bcrypt('password')
        ]);
    }

    public static function setUpBeforeClass()
    {
        self::$endpoint = '/api/v1/password/forgot';
    }

    public static function tearDownAfterClass()
    {
        self::$endpoint = null;
    }

    /**
     * Tests the POST /password/forgot route
     * Checks if the correct response statuses are returned
     * when requesting a password reset
     *
     * @return void
     */
    public function test_forgot_password_response_status()
    {
        // Given

        // I have an existing user who requests a password reset

        // Then
        // The response status should be 200
        // The message should be correct
        $this
            ->json(
                'POST',
                self::$endpoint,
                [
                    'email' => 'gaga@example.com',
                ],
                ['Accept' => 'application/json']
            )
            ->assertStatus(200)
            ->assertJson([
                'message' => 'A password reset link has been emailed.'
            ]);

        // And
        // I try to reset an invalid account (email)
        // The response status should be 404 Not Found
        $this
            ->json(
                'POST',
                self::$endpoint,
                [
                    'email' => 'invalid_email@example.com',
                ],
                ['Accept' => 'application/json']
            )
            ->assertStatus(401);
    }
}
