<?php

namespace Tests\Api;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions as Transaction;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use Transaction;

    private static $endpoint;

    public function setUp()
    {
        parent::setUp();

        factory(User::class)->create([
            'email' => 'gaga@example.com',
            'password' => bcrypt('password')
        ]);
    }

    public static function setUpBeforeClass()
    {
        self::$endpoint = '/api/v1/login';
    }

    public static function tearDownAfterClass()
    {
        self::$endpoint = null;
    }

    /**
     * Tests the POST /login route
     * Successful login
     *
     * @return void
     */
    public function test_can_login_a_user()
    {
        // Given

        // I have an existing user...

        // ... who tries to log in
        $body = [
            'email' => 'gaga@example.com',
            'password' => 'password'
        ];

        // And I login the user
        // Then
        // The response status should be 200
        // The JSON should contain the correct keys
        // The message should be correct
        $response = $this->json('POST', self::$endpoint, $body, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['message', 'access_token'])
            ->assertJson([
                'message' => "User {$body['email']} logged in successfully"
            ]);
        $access_token = $response->getData()->access_token;

        // And the client should be able to access a guarded route
        $this
            ->withHeaders([
                'Authorization' => "Bearer {$access_token}"
            ])
            ->json('GET', '/api/v1/user')
            ->assertJsonStructure(['data' => ['id', 'name', 'email', 'created_at', 'updated_at']])
            ->assertStatus(200);
    }

    /**
     * Tests the POST /login route
     * Failed login
     *
     * @return void
     */
    public function test_login_fails_if_fields_are_invalid()
    {
        // Given

        // I have an existing user...

        // ... and various scenarios for logging in
        $scenarios = [
            // Missing email
            [
                'email' => '',
                'password' => 'password'
            ],
            // Missing password
            [
                'email' => 'gaga@example.com',
                'password' => ''
            ],
            // Wrong password
            [
                'email' => 'gaga@example.com',
                'password' => 'password0'
            ],
            // Inexistent user
            [
                'email' => 'gagagaga@example.com',
                'password' => 'password'
            ]
        ];

        // And I try to login using the various scenarios
        // Then
        // The response status should be 401
        // The message should be correct
        array_walk($scenarios, function ($body) {
            $this->json('POST', self::$endpoint, $body, ['Accept' => 'application/json'])
                ->assertStatus(401)
                ->assertJson([
                    'message' => 'Email/password not recognized.'
                ]);
        });
    }
}
