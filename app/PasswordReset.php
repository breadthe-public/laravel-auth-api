<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $fillable = [
        'email', 'password', 'password_confirmation', 'token'
    ];

    // Override the default id key with token for the password_resets table
    public function getRouteKeyName()
    {
        return 'token';
    }
}
