<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;

class UserController extends Controller
{
    public function index(User $user)
    {
        try {
            return response()->json(['data' => Auth::user()], 200);
        } catch (AuthenticationException $e) {
            return response()->json(null, 401);
        }
    }
}
