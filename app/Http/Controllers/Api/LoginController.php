<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use App\PasswordReset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class LoginController extends Controller
{
    // Customize the validation messages
    public function messages()
    {
        return [
            'email.required' => 'An email is required',
            'email.email' => 'The email format is wrong',
            'password.required' => 'A password is required',
            'password.string' => 'The password must be a string',
            'password.min' => 'The password must be at least 6 characters long',
            'password_confirmation.required' => 'The password confirmation is missing',
            'password_confirmation.same' => 'The password must match the password confirmation',
        ];
    }

    /**
     * Login a user
     * POST /api/login
     */
    public function login(Request $request)
    {
        // Attempt to log in the user
        // if (Auth::attempt(['email' => request('email'), 'password' => request('password')])){
        if (Auth::once([
            'email' => request('email'),
            'password' => request('password')
        ])) {
            $user = Auth::user();

            $message = 'User ' . $user->email . ' logged in successfully';

            // Generate a new token; use the APP_NAME parameter as the token name
            $access_token = $user->createToken(config('app.name'))->accessToken;

            // Return a 200 success message
            return response()->json(compact('message', 'access_token'), 200);
        } else {
            return response()->json(['message' => 'Email/password not recognized.'], 401);
        }
    }

    /**
     * Logs out a user
     * POST /api/logout
     * Requires bearer token to be passed
     */
    public function logout(Request $request)
    {
        Auth::user()->token()->revoke();
        Auth::user()->token()->delete();
        return response()->json(null, 204);
    }

    /**
     * Requests a password reset link
     * POST /api/password/forgot
     */
    public function forgotPassword()
    {
        $email = request('email');

        // Find the user who owns the email
        if ($user = User::where(compact('email'))->first()) {
            // Generate a reset token
            $token = str_random(32);

            // Add the current timestamp
            $created_at = Carbon::now();

            // Store it in the password_resets table
            DB::table('password_resets')->insert(compact('email', 'token', 'created_at'));

            // Notify the user
            $user->sendPasswordResetNotification($token);

            return response()->json(['message' => 'A password reset link has been emailed.'], 200);
        }

        // The email doesn't exist, so return a 401 Unauthorized
        return response()->json(null, 401);
    }

    public function resetPassword()
    {
        $email = request('email');
        $password = request('password');
        $password_confirmation = request('password_confirmation');
        $token = request('token');

        // Validate the data
        $validator = Validator::make(request()->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
            'password_confirmation' => 'required|same:password',
        ], $this->messages());

        // If the validation fails return a 400 status along with the validation errors
        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(compact('message'), 400);
        }

        // Locate the password reset record
        $passwordReset = PasswordReset::where(compact('email', 'token'))->first();

        if ($passwordReset) {
            // Check if the token has expired
            if (Carbon::now()->gt($passwordReset->created_at->addDay())) {
                // Remove the password reset record
                // TODO: find a better way
                PasswordReset::where(compact('email', 'token'))->delete();

                return response()->json(['message' => 'The password reset token has expired.'], 401);
            }

            // Change the password
            User::where(compact('email'))
                ->update(['password' => bcrypt($password)]);

            // Remove the password reset record
            PasswordReset::where(compact('email', 'token'))->delete();

            // Respond with success
            return response()->json(['message' => 'Password was successfully reset.'], 200);
        }

        return response()->json(['message' => 'Invalid email or token.'], 401);
    }
}
