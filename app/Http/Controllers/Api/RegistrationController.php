<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\Welcome;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class RegistrationController extends Controller
{
    // Customize the validation messages
    public function messages()
    {
        return [
            'email.required' => 'An email is required',
            'email.email' => 'The email format is wrong',
            'email.unique' => 'This email already exists',
            'password.required' => 'A password is required',
            'password.string' => 'The password must be a string',
            'password.min' => 'The password must be at least 6 characters long',
            'password_confirmation.required' => 'The password confirmation is missing',
            'password_confirmation.same' => 'The password must match the password confirmation',
        ];
    }

    /**
     * Register a new user
     * POST /api/register
     */
    public function register(Request $request)
    {
        // Validate the registration fields
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6',
            'password_confirmation' => 'required|same:password',
        ], $this->messages());

        // If the validation fails return a 400 status along with the validation errors
        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(compact('message'), 400);
        }

        // Otherwise create the new user
        $user = User::create([
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

        /**
         * Optionally issue the registered user with a token (log him in)
         * Generate a new token; use the APP_NAME parameter as the token name
         */
        // $token = $user->createToken(config('app.name'))->accessToken;

        // Send the new user an email confirmation
        Mail::to($user)->send(new Welcome($user));

        // Return a 200 success message
        return response()->json("User {$user->email} registered successfully", 200);
    }
}
