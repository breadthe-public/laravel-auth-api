<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    // Redirect unauthorized requests (invalid token) to the root with a 401 response
    Route::get('/', function (Request $request) {
        return response()->json(null, 401);
    })->name('login');

    Route::post('/register', 'Api\RegistrationController@register');
    Route::post('/login', 'Api\LoginController@login');

    Route::post('/password/forgot', 'Api\LoginController@forgotPassword');
    Route::post('/password/reset', 'Api\LoginController@resetPassword');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/user', 'UserController@index');
        Route::post('/logout', 'Api\LoginController@logout');
    });
});
